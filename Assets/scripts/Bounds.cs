﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounds : MonoBehaviour {

    public int birdLeftPenaltySec;
    public int birdLeftPenaltyPts;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("Collision");
        print(collision.gameObject.tag);
        if(collision.gameObject.tag == "bird")
        {
            Destroy(collision.gameObject);
            Static.instance.ReduceTimer(birdLeftPenaltySec);
            Static.instance.InstantiateBird();
            Static.instance.AddSpeed(Static.instance.speedInc);
        }

    }

}
