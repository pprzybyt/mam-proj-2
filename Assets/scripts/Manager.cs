﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Manager : MonoBehaviour {

    public int birdPenaltySec;
    public int birdPenaltyPts;
    public int birdRewartSec;
    public int birdRewardPts;

    private RaycastHit2D hit;

    private BonusManager bonusManager;

    private void Start()
    {
        Static.instance.InstantiateBird();
        bonusManager = GetComponent<BonusManager>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            if (Input.touchCount > 0 || Input.GetMouseButton(0))
            {
                Vector2 mouse = Input.mousePosition;
                Vector2 touch = Input.touchCount > 0 ? Input.GetTouch(0).position : mouse;

                hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touch), Vector2.zero);
                try
                {
                    if (hit.collider.tag == "bird")
                    {
                        Static.instance.score += birdRewardPts;
                        Static.instance.ReduceTimer(-birdRewartSec);
                        print("killed bird");
                        Static.instance.ShowBlood(hit.collider.gameObject.transform.position);
                        Destroy(hit.collider.gameObject);
                        Static.instance.InstantiateBird();
                        Static.instance.AddSpeed(Static.instance.speedInc);
                    }
                    else if (hit.collider.tag == "bonus")
                    {
                        bonusManager.activate(hit.collider.gameObject);
                        Destroy(hit.collider.gameObject);
                    }
                }
                catch (NullReferenceException e)
                {
                    Static.instance.score -= birdRewardPts;
                    Static.instance.ReduceTimer(birdPenaltySec);
                }
            }
    }



}
