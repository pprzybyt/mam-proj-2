﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FlyController : MonoBehaviour {

    public Rigidbody2D rb;
    public float force = 1f;
    float coef = 10.0f;

    void Start () {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.right * Static.instance.speed * Time.deltaTime);
        float factor = coef * Static.instance.speed * -.1f;
        Vector3 forceVect = new Vector3(0, UnityEngine.Random.Range(-factor, factor), 0);
        rb.AddForce(forceVect * force);
       
    }          

}
