﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Static : MonoBehaviour {

    public static Static instance;
    public float speed;
    public float maxSpeed = 10.0f;
    public float speedInc;

    public GameObject pointsContainer;
    private Transform[] points;

    public GameObject bird;
    public GameObject blood;

    public int timer;
    public int score;

    void OnEnable () {
        instance = this;
        points = pointsContainer.GetComponentsInChildren<Transform>();
        InvokeRepeating("TimerClock", 0, 1);
    }

    public void InstantiateBird()
    {
        int idx = Random.Range(1, points.Length - 1);
        Instantiate(bird, points[idx].position, points[idx].rotation);
    }

    public void ShowBlood(Vector3 position)
    {
        AddToScene(blood, position);
    }

    public void AddToScene(GameObject obj, Vector3 position)
    {
        Instantiate(obj, position, Quaternion.identity);
    }

    public void TimerClock()
    {
        ReduceTimer(1);
    }

    public void ReduceTimer(int diff)
    {
        timer = Mathf.Max(timer - diff, 0);
    }

    public void AddSpeed(float diff)
    {
        speed = Mathf.Max(1, Mathf.Min(maxSpeed, speed + diff));
    }
}
