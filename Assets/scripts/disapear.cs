﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disapear : MonoBehaviour {
    Renderer rend;
    public float disapear_time = 5f;
    float speed;

    // Use this for initialization
    void Start () {
        rend = GetComponent<Renderer>();
        speed = 1 / disapear_time;
        StartCoroutine("DestroyObject");
    }
	
	// Update is called once per frame
	void Update () {
        Color c = rend.material.color;
        c.a -= Time.deltaTime * speed;
        rend.material.SetColor("_Color", c);
	}

    IEnumerator DestroyObject()
    {
        print("wait for: " + disapear_time);
        yield return new WaitForSeconds(disapear_time);
        print("Destroying..." + gameObject);
        Destroy(gameObject);
    }

}
