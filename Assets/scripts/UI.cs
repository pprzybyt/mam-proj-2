﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class UI : MonoBehaviour {

    public Text timerText;
    public Text scoreText;
    public Button pauseButton;
    public Button resumeButton;
    public Button restartButton;
    public Button exitButton;
    public Button exitButtonPause;
    public GameObject pauseScreen;
    public GameObject gameOverScreen;



    public void Start()
    {
        pauseScreen.SetActive(false);
        gameOverScreen.SetActive(false);

        pauseButton.onClick.AddListener(PauseButtonClick);
        resumeButton.onClick.AddListener(ResumeButtonClick);
        restartButton.onClick.AddListener(RestartButtonClick);
        exitButton.onClick.AddListener(ExitButtoClick);
        exitButtonPause.onClick.AddListener(ExitButtoClick);

    }

    private void RestartGame()
    {
        Static.instance.timer = 10;
        Static.instance.score = 0;
        Static.instance.speed = 1.0f;
    }

    private void ExitButtoClick()
    {
        pauseScreen.SetActive(false);
        Time.timeScale = 0;
        RestartGame();
        SceneManager.LoadScene(0);
    }

    private void RestartButtonClick()
    {
        RestartGame();
        gameOverScreen.SetActive(false);
        Time.timeScale = 1;
    }

    private void ResumeButtonClick()
    {
        Time.timeScale = 1;
        pauseScreen.SetActive(false);
    }

    private void PauseButtonClick()
    {
        Time.timeScale = 0;
        pauseScreen.SetActive(true);
    }

    public void OnGUI()
    {
        timerText.text = "SCORE: " + Static.instance.score + "\ntime left: " + Static.instance.timer;
        if (Static.instance.timer <= 0)
        {
            if (!gameOverScreen.activeSelf)
            {
                scoreText.text = "" + Static.instance.score;
                gameOverScreen.SetActive(true);
            }
        }
    }
}
