﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class InfoScene : MonoBehaviour
{

    public Button exitButton;


    // Use this for initialization
    void Start()
    {
        exitButton.onClick.AddListener(ExitButtonClick);
    }

    private void ExitButtonClick()
    {
        SceneManager.LoadScene(0);
    }
}

