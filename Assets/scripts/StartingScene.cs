﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class StartingScene : MonoBehaviour {

    public Button startButton;
    public Button exitButton;
    public Button infoButton;


    // Use this for initialization
    void Start () {
        startButton.onClick.AddListener(StartButtonClick);
        exitButton.onClick.AddListener(ExitButtonClick);
        infoButton.onClick.AddListener(InfoButtonClick);


    }

    private void InfoButtonClick()
    {
        SceneManager.LoadScene(2);
    }

    private void ExitButtonClick()
    {
        Application.Quit();
    }

    private void StartButtonClick()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }

}
