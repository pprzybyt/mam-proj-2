﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusManager : MonoBehaviour {

    public GameObject timeAddPrefab;
    public GameObject timeSubPrefab;
    public GameObject thunderPrefab;
    public GameObject cloudPrefab;
    public GameObject deathPrefab;
    public GameObject slowPrefab;


    public float bonus_interval_min;
    public float bonus_interval_max;
    private float bonus_interval;

    public int timeBonusUp;
    public int timeBonusDown;

    private Camera camera;

    private int x_lim = 2;
    private int y_lim = 4;

    private List<GameObject> bonuses;
    // Use this for initialization
    void Start () {
        camera = Camera.main;
        bonuses = new List<GameObject>();
        bonuses.Add(timeAddPrefab);
        bonuses.Add(timeSubPrefab);
        bonuses.Add(thunderPrefab);
        bonuses.Add(cloudPrefab);
        bonuses.Add(deathPrefab);
        bonuses.Add(slowPrefab);


        StartCoroutine("AddBonus");
    }

    public void activate(GameObject bonus)
    {
        switch (bonus.GetComponent<Bonus>().action)
        {
            case "shake":
                StartCoroutine(Shake(2f, 0.4f));
            break;
            case "clear-blood":
                var Bloods = GameObject.FindGameObjectsWithTag("blood");
                foreach (GameObject item in Bloods)
                {
                    Destroy(item);
                }
                break;
            case "game-over":
                Static.instance.ReduceTimer(Static.instance.timer);
                break;
            case "time-down":
                Static.instance.ReduceTimer(timeBonusDown);
                break;
            case "time-up":
                Static.instance.ReduceTimer(-timeBonusUp);
                break;
            case "slow":
                Static.instance.AddSpeed(-1);
                break;
            default:
                print("unknown bonnus");       
            break;

        }

    }

    IEnumerator AddBonus()
    {
        bonus_interval = Random.Range(bonus_interval_min, bonus_interval_max);
        yield return new WaitForSeconds(bonus_interval);
        System.Random r = new System.Random();
        int index = r.Next(0, bonuses.Count);

        Static.instance.AddToScene(bonuses[index], new Vector3(Random.Range(-x_lim, x_lim), Random.Range(-y_lim, y_lim), 0));
        StartCoroutine("AddBonus");
    }

    public IEnumerator Shake(float duration, float magnitude)
    {
        Vector3 orignalPosition = camera.transform.position;
        float elapsed = 0f;

        while (elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;
            camera.transform.position = new Vector3(x, y, -10f);
            elapsed += Time.deltaTime;
            yield return 0;
        }
        camera.transform.position = orignalPosition;
    }
}
